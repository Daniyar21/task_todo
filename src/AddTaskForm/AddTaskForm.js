import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = () => {
    return (
        <div>
            <form>
                <input type="text" className= 'addInput' placeholder={'Add new task'} />
                <button>Add</button>
            </form>
        </div>
    );
};

export default AddTaskForm;