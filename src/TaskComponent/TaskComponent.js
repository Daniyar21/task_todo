import React from 'react';

const Tasks = props => {
    return (
        <div className="tasks">
            <p>{props.task.text}</p>
            <span>{props.task.id}</span>
            <p><button onClick={props.onDelete}>X</button></p>
        </div>
    );
};

export default Tasks;