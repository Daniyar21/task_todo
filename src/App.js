import './App.css';
import AddTaskForm from "./AddTaskForm/AddTaskForm";
import {useState} from "react";
import Tasks from "./TaskComponent/TaskComponent";

function App() {

    const [tasks, setTasks] = useState( [
        {text: 'Buy milk', id: 1},
        {text: 'Walk with dog', id: 2},
    ]);

    const removeTask = (id) => {
        const tasksCopy = [...tasks];
        const index = tasks.findIndex(t=>t.id === id);
        tasksCopy.splice(index,1);
        setTasks(tasksCopy);

    }

    const taskComponents = tasks.map((t, i) =>(
        <Tasks task={t}
               onDelete = {()=>removeTask(t.id)}
               key = {t.id}
        />
    ))

  return (
    <div className="App">
      <AddTaskForm/>
        {taskComponents}
    </div>
  );
}

export default App;
